#include <WiFi.h>
#include <SPI.h>
#include "ThingSpeak.h" //includes libraries needed

int sensorPin = A1;
int sensorRead = 0;
int temp = 0;
int power = 8; //sets up variables used
char ssid[] = "PLUSNET";
char pass[] = "";
int status = WL_IDLE_STATUS; //set up for internet connection
const unsigned long channel_id = 1205291;
const char write_api_key[] = "W4X3D87Z2HCKGW93";
const char read_api_key[] = "PBDGD4XULZ4Z8E5B"; //used for thingspeak connection
WiFiClient client; //used for posting getting data from thingspeak

void setup() {
  pinMode(power, OUTPUT);
  pinMode(sensorPin, INPUT);

  Serial.begin(9600);
  Serial.println("Attempting to connect to WPA network...\n");
  status = WiFi.begin(ssid, pass);
  // if you're not connected, stop here:
  if ( status != WL_CONNECTED) {
    Serial.println("Couldn't get a wifi connection"); //sets up pins for sensor and pump control then starts wifi
  }
  else {
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("with IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println("Connected to network\n");
    delay(1000); //successful connection message
  }
  ThingSpeak.begin(client);
  Serial.print("System Initiating...\n");
  delay(1000); //begins thingspeak
}
void loop() {
  digitalWrite(power, LOW);
  sensorRead = analogRead(sensorPin); //reads moisture level
  sensorRead = sensorRead / 10.23; //converts to 100 for easier use
  Serial.print("\nPercent\n");  //used for de bugging:
  Serial.print(sensorRead); //
  delay(1000);
  if (sensorRead <= 27) {
    Serial.print("\nI'M FULL OF WATER!!\n");
    delay(1000);
  }
  if (sensorRead > 27 && sensorRead <= 59) {
    Serial.print("\nSHOULD BE FINE WITHOUT WATER\n");
    delay(1000);
  }
  if (sensorRead > 59) {
    Serial.print("\nI NEED WATER\n");
    delay(1000); //controls for measurement of moisture
  }
  delay(1000);
  Serial.print("\nSubmitting Reading please wait....\n");
  ThingSpeak.setField(1, String(sensorRead));
  delay(1000);
  Serial.print("Writing to Channel....\n");
  ThingSpeak.writeFields(channel_id, write_api_key); //posts data to thingspeak
  delay(5000);
  Serial.print("Write success!\n");
  delay(5000);
  Serial.print("\nTrying to get data from channel.....");
  temp = ThingSpeak.readIntField(channel_id, 1, read_api_key); //reads data from thingspeak
  delay(1000);
  Serial.print("\nRetrieving Data\n");
  delay(1000);
  Serial.print(temp);
  delay(1000);
  if (temp != sensorRead ){
    Serial.print("\nDATA ZERO! NO DATA RETRIEVED\n"); //error check
  }
  else{
  Serial.print("\nData Retrieved\n");
  delay(1000);
  if (temp >= 75) {
    Serial.print("Turning pump on"); //control and if statement for pump
    delay(1000);
    digitalWrite(power, HIGH);
    delay(1000);
    digitalWrite(power, HIGH);
  }
  }
  delay(1000);
  Serial.print("\nPROCESS COMPLETE, FLUSHING DATA....");
  client.flush();
  delay(1000);
   Serial.print("\nFLUSHING COMPLETE, STOPPING CLIENT....");
  client.stop();
   delay(1000);
    Serial.print("\nRestarting.....\n"); //clean up of client data then end
}

